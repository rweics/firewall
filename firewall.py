#!/usr/bin/env python

from main import PKT_DIR_INCOMING, PKT_DIR_OUTGOING
from struct import pack, unpack
from socket import inet_ntoa

# TODO: Feel free to import any Python standard moduless as necessary.
# (http://docs.python.org/2/library/)
# You must NOT use any 3rd-party libraries, though.

class Firewall:
    def __init__(self, config, iface_int, iface_ext):
        self.iface_int = iface_int
        self.iface_ext = iface_ext

        # TODO: Also do some initialization if needed.
        self.geoipdb = list() # Sample record: ['77.240.144.0', '77.240.175.255', 'RU']
        self.ip_rules = list() # Sample rule: ['PASS', 'UDP', ('8.8.8.8', '8.8.8.8'), ('53', '54'), 3]
        self.dns_rules = list() # Sample rule: ['DROP', 'DNS', ['*', 'STANFORD', 'EDU']]
        self.log_rules = list() # Sample rule: ['LOG', 'TCP', ['*', 'BERKELEY', 'EDU']]
        self.tcp_connections = {} # To store HTTP headers of requests and responses

        # TODO: Load the firewall rules (from rule_filename) here.
        self.verdicts = ['PASS', 'DROP']
        self.protocols = {6:'TCP', 17:'UDP', 1:'ICMP'}
        rules = open(config['rule'])
        index = 0
        for line in rules:
            line = line.strip()
            if (line != '' and line[0] != '%'):
                rule = line.upper().split()
                if rule[1] in self.protocols.values() or rule[1] == 'HTTP':
                    ip = rule[2].split('/')
                    if rule[0] == 'LOG':
                        rule[2] = rule[2].split('.')
                        self.log_rules.append(rule)
                    else:
                        if len(ip) == 1:
                            rule[2] = (ip[0], ip[0])
                        else: rule[2] = self.get_range(ip[0], int(ip[1]))
                        port = rule[3].split('-')
                        if len(port) == 1:
                            rule[3] = (port[0], port[0])
                        else: rule[3] = (port[0], port[1])
                        rule.append(index)
                        self.ip_rules.append(rule)
                if rule[1] == 'DNS':
                    rule.append(index)
                    rule[2] = rule[2].split('.')
                    self.dns_rules.append(rule)
            index += 1

        rules.close()

        # TODO: Load the GeoIP DB ('geoipdb.txt') as well.
        geoipdb = open('geoipdb.txt')
        for line in geoipdb:
            self.geoipdb.append(line[0:len(line)-1].split(' '))
        geoipdb.close()

        self.log_file = open('http.log', 'a')

    # @pkt_dir: either PKT_DIR_INCOMING or PKT_DIR_OUTGOING
    # @pkt: the actual data of the IPv4 packet (including IP header)
    def handle_packet(self, pkt_dir, pkt):
        # TODO: Your main firewall code will be here.
        protocol_number = ord(pkt[9])
        protocol = self.protocols[protocol_number] if protocol_number in self.protocols else None
        ip_payload_offset = (ord(pkt[0]) & 15) * 4 # IHL field, integer
        remote_port = None # in case it is icmp packet
        internal_port = None

        # Parsing IP packet
        if pkt_dir == PKT_DIR_OUTGOING:
            remote_ip_str = '.'.join([str(ord(c)) for c in list(pkt[16:20])]) # string
            if (protocol == 'TCP' or protocol == 'UDP'):
                remote_port = 256*ord(pkt[ip_payload_offset+2])+ord(pkt[ip_payload_offset+3]) # integer
                internal_port = 256*ord(pkt[ip_payload_offset])+ord(pkt[ip_payload_offset+1])
        elif pkt_dir == PKT_DIR_INCOMING:
            remote_ip_str = '.'.join([str(ord(c)) for c in list(pkt[12:16])]) # string
            if (protocol == 'TCP' or protocol == 'UDP'):
                remote_port = 256*ord(pkt[ip_payload_offset])+ord(pkt[ip_payload_offset+1]) # integer
                internal_port = 256*ord(pkt[ip_payload_offset+2])+ord(pkt[ip_payload_offset+3])
        if protocol == 'ICMP': remote_port = ord(pkt[ip_payload_offset]) # Treating ICMP Type the same way as tcp/udp port

        # Deciding whether passing or dropping the packet
        forward = self.check_ip_rules(protocol, remote_ip_str, remote_port)
        forward_overwrited = False
        # If it is a TCP HTTP packet
        if remote_port == 80  and protocol == 'TCP':
            seqno = unpack('!L', pkt[ip_payload_offset+4:ip_payload_offset+8])[0]
# !!!!!!!!!!!!! DOES NOT DEAL WITH CASE SYN RETRANSMIT !!!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # Check if a syn packet and has not been 'DENY' or 'DROP'
            if ord(pkt[ip_payload_offset+13]) & 0x02 and forward[0] != 'DENY' and forward[0] != 'DROP':
                # If above and direction outgoing add to hashtable
                if pkt_dir == PKT_DIR_OUTGOING:
                    self.tcp_connections[internal_port] = Connection(seqno)
                else:
                    # If SYN + ACK update orig_res_seqno and res_seqno
                    self.tcp_connections[internal_port].orig_res_seqno = seqno
                    self.tcp_connections[internal_port].res_seqno = (seqno + 1) % 0x100000000
            # If not 'DENY' or 'DROP' and not syn look in hashtable and glue
            elif forward[0] != 'DENY' and forward[0] != 'DROP':
            # Check if received host_name
                if internal_port in self.tcp_connections:
                    connection = self.tcp_connections[internal_port]
                    # Update forward according to return of func glue
                    if pkt_dir == PKT_DIR_OUTGOING:
                        in_order = connection.glue_req(pkt, ip_payload_offset)
                    else:
                        in_order = connection.glue_res(pkt, ip_payload_offset)
                    forward = ('PASS' if in_order else 'DROP', forward[1])
                    if connection.received_host():
                    # If received compare it with ip_rules
                        host_name = connection.host_name[:].upper().split('.')
                        # If does not match, delete entry from hashtable
                        if not self.check_log_rules(host_name):
                            del self.tcp_connections[internal_port]
                        else:
                            # If match with ip_rules, check if receive complete, then write to file
                            if connection.completed():
                                self.log_file.write(str(connection))
                                self.log_file.flush()
                                connection.clear()
# !!!!!!!!!!!!!!!!!!!! KEEEP ALLLLLIIIIVEEEEE !!!!!!!!!!!!!!!!!!!
        # If it is an outgoing DNS packet
        if (remote_port == 53 and protocol == 'UDP' and pkt_dir == PKT_DIR_OUTGOING):
            UDP_HEADER_LENGTH = 8
            dns_packet_offset = ip_payload_offset + UDP_HEADER_LENGTH
            QDcount = 256*ord(pkt[dns_packet_offset+4])+ord(pkt[dns_packet_offset+5]) # integer
            # Check if QDcount is 1
            if QDcount == 1:
                i = dns_packet_offset+12
                address = []
                while (pkt[i] != '\0'):
                    char_count = ord(pkt[i])
                    address.append(pkt[i+1:i+char_count+1].upper())
                    i += char_count+1
                QType = 256 * ord(pkt[i+1]) + ord(pkt[i+2])
                # Check if QType is 1 or 28
                if QType == 1 or QType == 28:
                    dns_forward = self.check_dns_rules(address)
                    if dns_forward[0] > forward[0]:
                        forward = dns_forward
                        forward_overwrited = True
        if forward[1] == 'PASS':
            if pkt_dir == PKT_DIR_OUTGOING:
                self.iface_ext.send_ip_packet(pkt)
            else:
                self.iface_int.send_ip_packet(pkt)
        elif forward[1] == 'DENY':
            if forward_overwrited:
                # If QType is AAAA drop the packet
                if QType == 1:
                    # Send dns reply to int
                    self.iface_int.send_ip_packet(self.make_dns_response(pkt, ip_payload_offset))
            else:
                # Send a deny packet to int
                self.iface_int.send_ip_packet(self.make_tcp_rst(pkt, ip_payload_offset))

    # TODO: You can add more methods as you want.

    # Returns a dns response pointing to 54.173.224.150, which is constructed by modifying the
    # original dns query packet. Takes in the original pkt and IP payload offset as arguments.
    def make_dns_response(self, pkt, offset):
        # Fixed target: 54.173.224.150
        answer = '\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x01\x00\x04\x36\xad\xe0\x96'
        # Update Total length field of IPv4 header
        ip_length = 256*ord(pkt[2])+ord(pkt[3]) + len(answer)
        pkt = pkt[:2] + chr(ip_length / 0x100) + chr(ip_length % 0x100) + pkt[4:]
        # Swap dst and src address in IPv4 header
        pkt = pkt[:12] + pkt[16:20] + pkt[12:16] + pkt[20:]
        # Swap dst and src port in UDP header
        pkt = pkt[:offset] + pkt[offset+2:offset+4] + pkt[offset:offset+2] + pkt[offset+4:]
        # Update length of UDP header
        udp_length = 256*ord(pkt[offset+4])+ord(pkt[offset+5]) + len(answer)
        pkt = pkt[:offset+4] + chr(udp_length / 0x100) + chr(udp_length % 0x100) + pkt[offset+6:]
        # Update offset to dns_offset
        dns_offset = offset + 8
        # Update DNS flag
        pkt = pkt[:dns_offset+2] + '\x81\x80' + pkt[dns_offset+4:]
        # Update Answer count to 1
        pkt = pkt[:dns_offset+6] + '\x00\x01' + pkt[dns_offset+8:]
        # Append the answer at the end of DNS packet
        pkt += answer
        return self.update_ip_checksum(self.update_udp_checksum(pkt, offset), offset)

    # Construct a TCP packet with RST flag set, based on original TCP packet
    def make_tcp_rst(self, pkt, ip_header_length):
        # Swap dst and src address in IPv4 header
        pkt = pkt[:12] + pkt[16:20] + pkt[12:16] + pkt[20:]
        # Swap dst and src port in TCP header
        pkt = pkt[:ip_header_length] + pkt[ip_header_length+2:ip_header_length+4] + pkt[ip_header_length:ip_header_length+2] + pkt[ip_header_length+4:]
        # Omit TCP payload and change window size to 0
        pkt = pkt[:ip_header_length+14]+'\x00\x00'+pkt[ip_header_length+16:ip_header_length+20]
        # Update IP length
        tcp_len = len(pkt)
        pkt = pkt[:2] + chr(tcp_len/0x100)+chr(tcp_len%0x100) + pkt[4:]
        # Update TCP header length and set RST flag as well as ACK flag
        pkt = pkt[:ip_header_length+12]+'\x50\x14'+pkt[ip_header_length+14:]
        # Update the acknowledgement num to seq num + 1
        ack_num = unpack('!L', pkt[ip_header_length+4:ip_header_length+8])[0] + 1
        ack_num = ack_num % 0x100000000
        pkt = pkt[:ip_header_length+4] + pkt[ip_header_length+8:ip_header_length+12] + pack('!L', ack_num) + pkt[ip_header_length+12:]
        # Compute TCP checksum
        tcp_length = 20
        pseudo_header = pkt[12:20]+'\x00'+pkt[9]+chr(tcp_length / 0x100)+chr(tcp_length % 0x100)
        tcp_header = pkt[ip_header_length:ip_header_length+16]+pkt[ip_header_length+18:]
        checksum = self.compute_checksum(pseudo_header+tcp_header)
        tcp_pkt = pkt[:ip_header_length+16]+ chr(checksum / 0x100) + chr(checksum % 0x100) +pkt[ip_header_length+18:]
        return self.update_ip_checksum(tcp_pkt, ip_header_length)

    # Recompute checksum of IP packet, and return the updated packet
    def update_ip_checksum(self, pkt, ip_header_length):
        ip_header = pkt[:10]+pkt[12:ip_header_length]
        checksum = self.compute_checksum(ip_header)
        return pkt[:10] + chr(checksum / 0x100) + chr(checksum % 0x100) + pkt[12:]

    def update_udp_checksum(self, pkt, ip_header_length):
        udp_length = len(pkt)-ip_header_length
        pseudo_header = pkt[12:20]+'\x00'+pkt[9]+chr(udp_length / 0x100)+chr(udp_length % 0x100)
        udp_content = pkt[ip_header_length:ip_header_length+6]+pkt[ip_header_length+8:]
        checksum = self.compute_checksum(pseudo_header+udp_content)
        return pkt[:ip_header_length+6]+ chr(checksum / 0x100) + chr(checksum % 0x100) +pkt[ip_header_length+8:]

    def compute_checksum(self, content):
        checksum = 0
        #The main loop adds up each set of 2 bytes. They are first converted to strings and then concatenated
        #together, converted to integers, and then added to the sum.
        while len(content) > 1:
            checksum += ord(content[0]) * 256 + ord(content[1])
            content = content[2:]

        if len(content): #This accounts for a situation where the header is odd
            checksum += ord(content[0])

        checksum = (checksum & 0xffff) + (checksum >> 16)
        checksum = (~(checksum + (checksum >> 16))) & 0xFFFF
        return checksum

    # Return a tuple which first elem is line num of applied rule
    # and second elem is True for 'pass', False for 'drop'
    # Sample output: ('8', 'PASS')
    def check_ip_rules(self, protocol, ip_str, port):
        result = (-1, 'PASS')
        for rule in self.ip_rules:
            if protocol == rule[1]:
                if self.ip_in_range(ip_str, rule[2]):
                    if self.port_in_range(port, rule[3]):
                        result = (rule[-1], rule[0])
        return result

    # Return a tuple which first elem is line num of applied rule
    # and second elem is True for 'PASS', False for 'drop'
    # Sample input: ['GOOGLE', 'COM']
    # Sample output: ('5', 'DENY')
    def check_dns_rules(self, address):
        result = (-1, 'PASS')
        applies = False
        for rule in self.dns_rules:
            if rule[2][0] == '*' and len(rule[2]) <= len(address):
                applies = True
                for i in range(len(rule[2])-1):
                    if rule[2][-i-1] != address[-i-1]:
                        applies = False
                        break
                if applies:
                    result = (rule[-1], rule[0])
            elif rule[2] == address:
                result = (rule[-1], rule[0])
        return result

    # Return a boolean whether host_name matches any log rule
    def check_log_rules(self, host_name):
        for rule in self.log_rules:
            if rule[2][0] == '*':
                if len(rule[2]) == 1:
                    return True
                if len(rule[2]) <= len(host_name):
                    for i in range(len(rule[2])-1):
                        if rule[2][-i-1] != host_name[-i-1]:
                            return False
                    return True
            elif rule[2] == host_name:
                return True
        return False


    # Returns the country code that the ip address(string) belongs to.
    # In: '152.62.44.55'   Out: 'US'
    def get_country(self, ip):
        upper = len(self.geoipdb)-1
        lower = 0
        while upper >= lower:
            index = (upper-lower)/2+lower
            record = self.geoipdb[index]
            cmp_start = self.compare_ip_str(record[0], ip)
            cmp_end = self.compare_ip_str(ip, record[1])
            if cmp_start <= 0 and cmp_end <= 0:
                return record[2]
            elif cmp_start > 0:
                upper = index-1
            elif cmp_end > 0:
                lower = index+1
        return None # Not found

    # Returns True if ip is in ip_range, returns False otherwise
    def ip_in_range(self, ip, ip_range):
        if ip_range[0] == 'ANY': return True
        if len(ip_range[0]) == 2: return self.get_country(ip) == ip_range[0].upper()
        ip = self.get_ip_from_str(ip)
        lower = self.get_ip_from_str(ip_range[0])
        upper = self.get_ip_from_str(ip_range[1])
        return lower <= ip and ip <= upper

    # Returns True if port is in port_range, returns False otherwise
    # In: 23, ('21', '24')   Out: True
    def port_in_range(self, port, port_range):
        if port_range[0] == 'ANY':
            return True
        return int(port_range[0]) <= port and port <= int(port_range[1])

    # Returns -1 if ip1 < ip2, returns 0 if ip1 == ip2, returns 1 if ip1 > ip2
    # both ip1 and ip2 are passed in as strings
    def compare_ip_str(self, ip1, ip2):
        if ip1 == ip2: return 0
        ip1 = [int(a) for a in ip1.split('.')]
        ip2 = [int(a) for a in ip2.split('.')]
        while len(ip1) != 0:
            a = ip1.pop(0)
            b = ip2.pop(0)
            if a != b: return -1 if a < b else 1

    # Returns a tuple of two strings containg starting ip address and ending ip address
    # Input ip is a string such as '12.23.34.45', mask represents number of bits, e.g. 7
    # To get ip range of '192.168.1.1/30', pass in ('192.168.1.1', 30) and will return:
    # ('192.168.1.0', '192.168.1.3')
    def get_range(self, ip_str, mask_digits):
        ip_as_int = self.get_ip_from_str(ip_str)
        mask_digits = 32 - mask_digits # number of least significant bits to turn on
        mask = (1 << mask_digits) - 1 # actual mask
        ip_lower_bound = ip_as_int & (~mask)
        ip_upper_bound = ip_as_int | mask
        return (self.get_str_from_ip(ip_lower_bound), self.get_str_from_ip(ip_upper_bound))

    # Returns an integer representation of IP address when a string representation is passed in
    # e.g. In: '192.168.1.1'   Out: 3232235777
    def get_ip_from_str(self, ip_str):
        ip = [int(a) for a in ip_str.split('.')]
        ip_as_int = 0
        while len(ip) != 0:
            ip_as_int += pow(256, 4-len(ip))*ip.pop(len(ip)-1)
        return ip_as_int

    # Returns a string representation of IP address when an integer representation is passed in
    # e.g. In: 3232235777   Out: '192.168.1.1'
    def get_str_from_ip(self, ip):
        ip_str = ''
        while ip > 0:
            temp = ip % 256
            ip = int(ip / 256)
            if ip != 0:
                ip_str = '.'+str(temp)+ip_str
            else: ip_str = str(temp)+ip_str
        return ip_str

# TODO: You may want to add more classes/functions as well.

class Connection:
    def __init__(self, init_seqno):
        self.orig_req_seqno = init_seqno
        self.req_seqno = (init_seqno + 1) % 0x100000000 # Next expected seqno
        self.req_buff = ''
        self.orig_res_seqno = None
        self.res_seqno = None
        self.res_buff = ''

        """ Below are items to be collected """

        # Items looking for in self.req_buff
        self.host_name = None
        self.method = None
        self.path = None
        self.version = None

        # Items looking for in self.res_buff
        self.status_code = None
        self.object_size = None

    # Returns a string that matches the format of a log record
    def __repr__(self):
        return self.host_name+'\t'+self.method+'\t'+self.path+'\t'+self.version+'\t'+self.status_code+'\t'+self.object_size+'\n'

    # Given a request packet, glue payload with existing buffer if not out of order, and return True for success
    # If sequence number is out of order, signal caller to drop the packet by returning False
    def glue_req(self, pkt, ip_header_length):
        # Return True immediately if we have collected everything we want, to save memory and time
        if self.host_name and self.method and self.path and self.version:
            return True
        seqno = unpack('!L', pkt[ip_header_length+4:ip_header_length+8])[0]
        # ackno = struct.unpack('!L', pkt[ip_header_length+8:ip_header_length+12])
        if not self.seq_in_range(seqno, True):
            return False
        if seqno == self.req_seqno:
            tcp_header_len = 4 * (ord(pkt[ip_header_length+12]) >> 4)
            pkt_buff = pkt[ip_header_length+tcp_header_len:]
            # Append pkt_buff to self.req_buff
            self.req_buff += pkt_buff
            self.req_seqno = (self.req_seqno + len(pkt_buff)) % 0x100000000
            # If last character is \n
            if pkt_buff == '':
                return True
            if self.req_buff[-1] == '\n':
                lines = self.req_buff.split('\n')
                self.req_buff = ''
            # If last char is not \n
            else:
                lines = self.req_buff.split('\n')
                self.req_buff = lines[-1]
                lines = lines[:-1]
            # If parsed new line, inspect if contains info in interest
            for line in lines:
                line = line.strip().split()
                # If line is empty line
                # If parsed end of header and does not contain host_name, fill host_name
                # with ext IP address
                if line == []:
                    if self.host_name == None:
                        self.host_name = inet_ntoa(pkt[16:20])
                    return True
                # If we do not have the first line
                if self.method == None:
                    self.method = line[0]
                    self.path = line[1]
                    self.version = line[2]
                else:
                    # Case 'host:google.com'
                    if len(line) == 1:
                        line = line.split(':')
                    if line[0].upper() == 'HOST:' or line[0].upper() == 'HOST':
                        self.host_name = line[-1]
        # Return true to pass the packet
        return True

    # Given a response packet, glue payload with existing buffer if not out of order, and return True for success
    # If sequence number is out of order, signal caller to drop the packet by returning False
    def glue_res(self, pkt, ip_header_length):
        # Return True immediately if we have collected everything we want, to save memory and time
        if self.status_code and self.object_size:
            return True
        seqno = unpack('!L', pkt[ip_header_length+4:ip_header_length+8])[0]
        # ackno = struct.unpack('!L', pkt[ip_header_length+8:ip_header_length+12])
        if not self.seq_in_range(seqno, False):
            return False
        if seqno == self.res_seqno:
            tcp_header_len = 4 * (ord(pkt[ip_header_length+12]) >> 4)
            pkt_buff = pkt[ip_header_length+tcp_header_len:]
            # Append pkt_buff to self.res_buff
            self.res_buff += pkt_buff
            self.res_seqno = (self.res_seqno + len(pkt_buff)) % 0x100000000
            # If last character is \n
            if pkt_buff == '':
                return True
            if self.res_buff[-1] == '\n':
                lines = self.res_buff.split('\n')
                self.res_buff = ''
            # If last char is not \n
            else:
                lines = self.res_buff.split('\n')
                self.res_buff = lines[-1]
                lines = lines[:-1]
            # If parsed new line, inspect if contains info in interest
            for line in lines:
                line = line.strip().split()
                # If line is empty line
                if line == []:
                    if self.object_size == None:
                        self.object_size = '-1'
                    return True
                # If we do not have the first line
                if self.status_code == None:
                    self.status_code = line[1]
                else:
                    # Case 'Content-Length:213'
                    if len(line) == 1:
                        line = line.split(':')
                    if line[0].upper() == 'CONTENT-LENGTH:' or line[0].upper() == 'CONTENT-LENGTH':
                        self.object_size = line[-1]
        # Return true to pass the packet
        return True

    # Returns true if received host_name, false otherwise
    def received_host(self):
        return self.host_name != None

    # Returns a boolean indicating whether or not all information needed has already been collected for this connection
    # If it's the first time when everything is collected, write log into log_file
    def completed(self):
        return self.host_name and self.method and self.path and self.version and self.status_code and self.object_size

    # Returns true if seqno is one of the received or expected packet, false otherwise
    # @param seqno: current sequence number
    # @param is_req: true if req, false if res
    def seq_in_range(self, seqno, is_req):
        orig_seqno = self.orig_req_seqno if is_req else self.orig_res_seqno
        curr_seqno = self.req_seqno if is_req else self.res_seqno
        if orig_seqno < curr_seqno:
            return seqno >= orig_seqno and seqno <= curr_seqno
        else:
            return seqno >= orig_seqno or seqno <= curr_seqno

    # Clear all fields to get ready for the next request/response pair
    def clear(self):

        """ NEED TO UPDATE "self.seqno += self.object_size" to skip HTTP body? """
        if self.object_size != '-1':
            self.res_seqno = (self.res_seqno + int(self.object_size)) % 0x100000000

        self.host_name = None
        self.method = None
        self.path = None
        self.version = None
        self.status_code = None
        self.object_size = None